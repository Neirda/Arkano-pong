#ifndef __PLAYER_H__
#define  __PLAYER_H__

#include "game.h"

typedef struct Player{
  int id;
  int life;
  struct Paddle * paddle;
  struct Bonus * bonusTab;
}Player;

Player initPlayer(int id, int life, struct Paddle * paddle, int nbBonusType);

#endif
