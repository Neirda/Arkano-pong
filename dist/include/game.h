#ifndef __GAME_H__
#define __GAME_H__

#include <stdlib.h>
#include <stdio.h>

#ifdef __APPLE__
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>
#else
    #include <GL/gl.h>
    #include <GL/glu.h>
#endif

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include <time.h>
#include <math.h>

#include "player.h"
#include "paddle.h"
#include "brick.h"
#include "ball.h"
#include "geometry.h"
#include "bonus.h"
#include "textures.h"

static unsigned int FRAMERATE_MILLISECONDS = 1000 / 120;
static unsigned int WINDOW_WIDTH = 800;
static unsigned int WINDOW_HEIGHT = 600;
static const unsigned int BIT_PER_PIXEL = 32;

typedef struct{
  char key[SDLK_LAST];
  char quit;
}Input;

typedef struct{
  Player ** players;
  int nbPlayers;
  int level;

  SDL_Surface* screen;
}Game;

void updateEvents(Input* in);
void reshape(int width, int height);

int initSDL(Game * game);

void playersMovements(Input * in, Player * player1, Player * player2);
void stopGame(Ball * ballPlayer1, Ball * ballPlayer2, Paddle * paddlePlayer1, Paddle * paddlePlayer2);

void printElements(Ball * ballPlayer1, Ball * ballPlayer2, Paddle * paddlePlayer1, Paddle * paddlePlayer2);

#endif
