#ifndef __PADDLE_H__
#define __PADDLE_H__

#include "game.h"

typedef struct Paddle{
  int xPos;
  int yPos;

  int vitesse;
  int acc;
  float friction;
  int vitesseMax;

  int xSize;
  int ySize;

  int red;
  int blue;
  int green;

  GLuint *texture;

  Player *player;
}Paddle;

Paddle initPaddle(int xPos, int yPos, int vitesse, int acc, float friction, int vitesseMax, int xSize, int ySize, int red, int green, int blue, GLuint * texture, Player * player);
void printPaddle(Paddle* p);

void stopPaddle(Paddle* p);
void slowPaddle(Paddle* p);

void movePaddleRight(Paddle* p, float WINDOW_WIDTH);
void movePaddleLeft(Paddle* p);

#endif
