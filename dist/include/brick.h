#ifndef __BRICK_H__
#define __BRICK_H__

#include "game.h"


typedef struct Brick{
    int xPos;
    int yPos;

    int xSize;
    int ySize;

    int type;

    int red;
    int green;
    int blue;

    int visible;
}Brick;

Brick initBrick(int xPos, int yPos, int xSize, int ySize, int type, int visible);
void printBrick(Brick* b);
void destroyBrick(Brick* brick);
Brick ** initBrickTab(int brickTabWidth, int brickTabHeight, int* brickTabTypes, int brickHeight, int WINDOW_WIDTH, int WINDOW_HEIGHT);
void printBrickTab(Brick ** brickTab, int brickTabHeight, int brickTabWidth);
void freeBrickTab(Brick ** brickTab, int brickTabWidth);
void readBrickFile(int* brickTabHeight, int* brickTabWidth, int* brickTabTypes);


#endif
