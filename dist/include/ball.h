#ifndef __BALL_H__
#define __BALL_H__

#include "game.h"

typedef struct Ball{
  float xPos;
  float yPos;

  float normeVitesse;
  float angleVitesse;

  float radius;

  int red;
  int blue;
  int green;

  Player * player;

  GLuint texture;
/* vitesse, bonus*/

}Ball;


Ball initBall(float xPos, float yPos, float normeVitesse, float angleVitesse, float radius, int red, int green, int blue, Player * player);
void printBall(Ball * ball);
void resetBall(Ball * ball);
void initDirectionBalls(Ball * ballPlayer1, Ball * ballPlayer2);

void checkWallCollision(Ball * ball, Player * Player1, Player * Player2, int WINDOW_WIDTH, int WINDOW_HEIGHT);
void checkPaddlesCollision(Ball * ball, Paddle * paddle1, Paddle * paddle2);
void checkBrickCollision(Ball * ball, Brick * brick);
void checkBricksCollision(Ball * ball, Brick ** brickTab, int brickTabHeight, int brickTabWidth);
void checkAllCollision(Player * player1, Player * player2, Ball * ballPlayer1, Ball * ballPlayer2, int WINDOW_WIDTH, int WINDOW_HEIGHT);


void setPositionBall(Ball * b, float xPos, float yPos);
void setNormeVitesseBall(Ball * ball, float normeVitesse);
void setAngleVitesseBall(Ball * ball, float angleVitesse);
void stopBall(Ball * ball);


#endif
