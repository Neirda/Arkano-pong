#ifndef __BONUS_H__
#define  __BONUS_H__

#include "game.h"

typedef struct Bonus{
    int timeValue;
    int type;
    Player* player;
}Bonus;

void handleBrick(Brick* brick, Ball* ball);
Bonus createBonus(int type, Player* player);
void setBonusTime(int timeValue, int type, Player* player);
void updateTimers(Bonus* bonusTab, int nbBonusType, int now, Ball* ball) ;
Bonus * initBonusTab(int nbBonusType, Player* player);
void destroyBonus(Bonus* bonus);
void scaleUpPaddle(Paddle* p);
void scaleDownPaddle(Paddle* p);
void speedUpBall(Ball* ball);
void slowDownBall(Ball* ball);
void lifeUp(Player* player);

#endif
