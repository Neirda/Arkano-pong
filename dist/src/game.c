#include "game.h"

void reshape(int width, int height){
      glViewport(0,0, width, height);
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      gluOrtho2D(0, 800, 800*height/width, 0);
}

int initSDL(Game * game){

  if(game == NULL){
    return -1;
  }

  if(-1 == SDL_Init(SDL_INIT_VIDEO)) {
      fprintf(stderr, "Impossible d'initialiser la SDL. Fin du programme : %s\n", SDL_GetError());
      return -1;
  }

  game->screen = NULL;

  if(NULL == (game->screen = SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, BIT_PER_PIXEL,
      SDL_DOUBLEBUF | SDL_OPENGL))) {
      fprintf(stderr, "Impossible d'ouvrir la fenetre. Fin du programme.\n");
      return -1;
  }

  SDL_WM_SetCaption("Arkanopong", NULL);

  reshape(WINDOW_WIDTH, WINDOW_HEIGHT);

  return 0;
}

void updateEvents(Input* in)
{
  SDL_Event event;

  while(SDL_PollEvent(&event))
  {
    switch(event.type)
    {
      case SDL_KEYDOWN:
        in->key[event.key.keysym.sym] = 1;
        break;

      case SDL_KEYUP:
        in->key[event.key.keysym.sym] = 0;
        break;

      case SDL_QUIT:
        in->quit = 1;
        break;

      default:
        break;
    }
  }
}

void playersMovements(Input* in, Player * player1, Player * player2){

  if(in->key[SDLK_RIGHT])
  {
    movePaddleRight(player1->paddle, WINDOW_WIDTH);
  }

  if(in->key[SDLK_LEFT])
  {
    movePaddleLeft(player1->paddle);
  }

  if(in->key[SDLK_a])
  {
    movePaddleLeft(player2->paddle);
  }

  if(in->key[SDLK_z])
  {
    movePaddleRight(player2->paddle, WINDOW_WIDTH);
  }

  slowPaddle(player1->paddle);
  slowPaddle(player2->paddle);

}

void stopGame(Ball * ballPlayer1, Ball * ballPlayer2, Paddle * paddlePlayer1, Paddle * paddlePlayer2){
  stopBall(ballPlayer1);
  stopBall(ballPlayer2);

  stopPaddle(paddlePlayer1);
  stopPaddle(paddlePlayer2);
}

void printElements(Ball * ballPlayer1, Ball * ballPlayer2, Paddle * paddlePlayer1, Paddle * paddlePlayer2){
  printBall(ballPlayer1);
  printBall(ballPlayer2);

  printPaddle(paddlePlayer1);
  printPaddle(paddlePlayer2);
}
