#include "game.h"


void loadTextures(Game* game) {

    /**
    * Chargement des textures //NE FONCTIONNE PAS
    */

    SDL_Surface* surfacePaddlePlayer1 = IMG_Load("medias/img/paddle.png");
    if (surfacePaddlePlayer1 == NULL) {
        printf("Erreur. Vérifier le chemin fourni.\n");
    }
    GLuint texturePaddlePlayer1;
    glGenTextures(1, &texturePaddlePlayer1);

    printf("texture : %d\n", texturePaddlePlayer1);

    glBindTexture(GL_TEXTURE_2D, texturePaddlePlayer1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    
    GLenum format;
    switch(surfacePaddlePlayer1->format->BytesPerPixel) {
        case 1:
            format = GL_RED;
        break;
        case 3:
            format = GL_RGB;
        break;
        case 4:
            format = GL_RGBA;
        break;
        default:
            fprintf(stderr, "Format des pixels de l’image non pris en charge\n");
            return;
        break;
    }
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surfacePaddlePlayer1->w, surfacePaddlePlayer1->h, 0, format, GL_UNSIGNED_BYTE, surfacePaddlePlayer1->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);



}
