#include "game.h"

Paddle initPaddle(int xPos, int yPos, int vitesse, int acc, float friction, int vitesseMax, int xSize, int ySize, int red, int green, int blue, GLuint * texture, Player * player){
  Paddle paddle;

  paddle.xPos = xPos;
  paddle.yPos = yPos;

  paddle.vitesse = vitesse;
  paddle.acc = acc;
  paddle.friction = friction;
  paddle.vitesseMax = vitesseMax;

  paddle.xSize = xSize;
  paddle.ySize = ySize;

  paddle.red   = red;
  paddle.green = green;
  paddle.blue  = blue;

  paddle.texture = texture;

  paddle.player = player;

  return paddle;
}

void printPaddle(Paddle* p){
  if(p != NULL)
  {
    int xMid = p->xSize/2;
    int yMid = p->ySize/2;

    glColor3ub(p->red, p->green, p->blue);


    // glEnable(GL_TEXTURE_2D );
    // glBindTexture(GL_TEXTURE_2D, p->texture);

    glBegin(GL_POLYGON);
    //   glTexCoord2f(0,1);
      glVertex2f(p->xPos - xMid, p->yPos - yMid);

    //   glTexCoord2f(1,1);
      glVertex2f(p->xPos + xMid, p->yPos - yMid);

    //   glTexCoord2f(1,0);
      glVertex2f(p->xPos + xMid, p->yPos + yMid);

    //   glTexCoord2f(0,0);
      glVertex2f(p->xPos - xMid, p->yPos + yMid);
    glEnd();

    // glBindTexture(GL_TEXTURE_2D, 0);
    // glDisable(GL_TEXTURE_2D);
  }
}

void stopPaddle(Paddle* p){
  if(p != NULL){
    p->vitesse = 0;
  }
}

void movePaddleRight(Paddle* p, float WINDOW_WIDTH){
    p->vitesse += p->acc;

    if (p->vitesse > p->vitesseMax) {
        p->vitesse = p->vitesseMax;
    }

    if (p->xPos + p->vitesse > WINDOW_WIDTH - p->xSize/2) {
        return;
    }

    p->xPos += p->vitesse;
}

void movePaddleLeft(Paddle* p){
    p->vitesse -= p->acc;

    if (p->vitesse < -p->vitesseMax) {
        p->vitesse = -p->vitesseMax;
    }

    if (p->xPos + p->vitesse < 0 + p->xSize/2) {
        return;
    }

    p->xPos += p->vitesse;
}

void slowPaddle(Paddle* p) {
    p->vitesse = (int) p->vitesse * p->friction;
}
