#include "game.h"

int main(int argc, char** argv) {

    Game game;
    /*Crée un tableau avec la gestion de chaque touche. Tout le tableau mis à 0 : aucune touche n'est appuyé*/
    Input in;
    memset(&in, 0, sizeof(in));
    if(initSDL(&game) != 0)
      return EXIT_FAILURE;



    /* INITIALISATION OF THE PLAYERS */
    Player player1;
    Paddle paddlePlayer1 = initPaddle(WINDOW_WIDTH/2, WINDOW_HEIGHT - 10, 0.0, 2, 0.99, 20, 100, 20, 10, 90, 255, 0, &player1);

    player1 = initPlayer(1, 5, &paddlePlayer1, 5);
    Ball ballPlayer1 = initBall(WINDOW_WIDTH/2, 3*WINDOW_HEIGHT/4, 2, M_PI/3, 10, 90, 190, 210, &player1);


    Player player2;
    Paddle paddlePlayer2 = initPaddle(WINDOW_WIDTH/2, 10, 0.0, 2, 0.99, 20, 100, 20, 155, 155, 155, 0, &player2);
    player2 = initPlayer(2, 5, &paddlePlayer2, 5);
    Ball ballPlayer2 = initBall(WINDOW_WIDTH/2, WINDOW_HEIGHT/4, 1, M_PI/3, 10, 240, 90, 80, &player2);



    int brickTabHeight = 0;
    int brickTabWidth = 0;
    int brickYSize = 20;
    int brickTabTypes[1024];

    /**
    * Ouverture Fichier
    */
    readBrickFile(&brickTabHeight, &brickTabWidth, brickTabTypes);

    /**
    * Initialisation Tableau de Briques
    */
    Brick ** brickTab = initBrickTab(brickTabWidth, brickTabHeight, brickTabTypes, brickYSize, WINDOW_WIDTH, WINDOW_HEIGHT);



    /* GAME AND EVENTS */
    int end = 0;


    while(!in.key[SDLK_ESCAPE] && !in.quit)
    {
      Uint32 startTime = SDL_GetTicks();

      glClear(GL_COLOR_BUFFER_BIT);


      /* DID ONE PLAYER WIN THE GAME ?*/
      if(end){
        if(player1.life == 0){
          printf("Player 2 win the game!\n");
        } else {
          printf("Player 1 win the game!\n");
        }
        in.quit = 1;
      }


      if(player1.life == 0 || player2.life == 0){
          end = 1;
          stopGame(&ballPlayer1, &ballPlayer2, &paddlePlayer1, &paddlePlayer2);
          /*Proposer de rejouer maybe*/
      }

      printElements(&ballPlayer1, &ballPlayer2, player1.paddle, player2.paddle);
      printBrickTab(brickTab, brickTabHeight, brickTabWidth);



      checkAllCollision(&player1, &player2, &ballPlayer1, &ballPlayer2, WINDOW_WIDTH, WINDOW_HEIGHT);
      checkBricksCollision(&ballPlayer1, brickTab, brickTabHeight, brickTabWidth);
      checkBricksCollision(&ballPlayer2, brickTab, brickTabHeight, brickTabWidth);

      updateTimers(player1.bonusTab, 5, SDL_GetTicks(), &ballPlayer1);
      updateTimers(player2.bonusTab, 5, SDL_GetTicks(), &ballPlayer2);

      SDL_GL_SwapBuffers();

      updateEvents(&in);
      playersMovements(&in, &player1, &player2);

      Uint32 elapsedTime = SDL_GetTicks() - startTime;
      if(elapsedTime < FRAMERATE_MILLISECONDS){
        SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
      }
    }

    /*Pas oublier de free tous les objets*/
    freeBrickTab(brickTab, brickTabWidth);

    SDL_Quit();

    return EXIT_SUCCESS;
}
