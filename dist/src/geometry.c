#include "game.h"

void drawRectangle(){
  glBegin(GL_POLYGON);
    glVertex2f(-0.5, -0.5);
    glVertex2f(0.5, -0.5);
    glVertex2f(0.5, 0.5);
    glVertex2f(-0.5, 0.5);
  glEnd();
}

void drawCircle(int full, float radius) {
  /** Dessine un cercle (plein ou non) de rayon 0.5 et centré en (0, 0). **/
  int resCircle = 64, i;
  float angle, x, y;

  if (full == 1) {
    glBegin(GL_POLYGON);
  }else{
    glBegin(GL_LINE_LOOP);
  }

  for (i = 0; i < resCircle; ++i ) {
    angle = i * (2*M_PI)/resCircle;

    x = cos(angle) * radius;
    y = sin(angle) * radius;

    glVertex2f(x, y);
  }

  glEnd();
}
