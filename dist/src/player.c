#include "game.h"

Player initPlayer(int id, int life, struct Paddle * paddle, int nbBonusType){
  Player player;

  player.id     = id;
  player.life   = life;
  player.paddle = paddle;
  player.bonusTab = initBonusTab(nbBonusType, &player);

  return player;
}
