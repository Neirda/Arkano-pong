#include "game.h"

void handleBrick(Brick* brick, Ball* ball){
    switch (brick->type) {
        case 2 :
            setBonusTime(SDL_GetTicks(), brick->type,ball->player);
            scaleUpPaddle(ball->player->paddle);
        break;
        case 3 :
            setBonusTime(SDL_GetTicks(), brick->type,ball->player);
            speedUpBall(ball);
        break;
        case 4 :
            setBonusTime(SDL_GetTicks(), brick->type,ball->player);
            scaleDownPaddle(ball->player->paddle);
        break;
        case 5 :
            setBonusTime(SDL_GetTicks(), brick->type,ball->player);
            lifeUp(ball->player);
        break;
    }
    if (brick->type == 1) return;
    destroyBrick(brick);
}

Bonus createBonus(int type, Player* player){
    Bonus bonus;
    bonus.timeValue = 0;
    bonus.type = type;
    bonus.player = player;
    return bonus;
}

Bonus * initBonusTab(int nbBonusType, Player* player){
    Bonus * bonusTab;
    bonusTab = malloc( nbBonusType * sizeof(Bonus));

    if (bonusTab == NULL)
    {
      exit(EXIT_FAILURE);
    }
    int i;
    for (i = 0; i < nbBonusType; i++)
    {
      bonusTab[i] = createBonus(nbBonusType, player);
    }

    return bonusTab;
}

void setBonusTime(int timeValue, int type, Player* player){
    player->bonusTab[type].timeValue = timeValue;
}

void updateTimers(Bonus* bonusTab, int nbBonusType, int now, Ball* ball) {
    int i;
    for (i = 0; i < nbBonusType; i++) {
        if ( now - (bonusTab[i]).timeValue > 10000 ) {
            switch (i) {
                case 2 :
                    scaleDownPaddle(ball->player->paddle);
                break;
                case 3 :
                    slowDownBall(ball);
                break;
                case 4 :
                    scaleUpPaddle(ball->player->paddle);
                break;
            }
            setBonusTime(now, i,ball->player);
        }
    }
}



void scaleUpPaddle(Paddle* p){
    p->xSize *= 1.2 ;
}
void scaleDownPaddle(Paddle* p){
    p->xSize /= 1.2 ;
}
void speedUpBall(Ball* ball){
    ball->normeVitesse *= 2;
}
void slowDownBall(Ball* ball){
    ball->normeVitesse /= 2;
}
void lifeUp(Player* player){
    player->life++;
}
