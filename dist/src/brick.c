#include "game.h"
#include <string.h>
Brick initBrick(int xPos, int yPos, int xSize, int ySize, int type, int visible){
  Brick brick;

  brick.xPos = xPos;
  brick.yPos = yPos;

  brick.xSize = xSize;
  brick.ySize = ySize;

  brick.type = type;

  brick.red   = (type+1)*40;
  brick.green = (type+1)*40;
  brick.blue  = (type+1)*40;

  brick.visible = visible;

  return brick;
}

void printBrick(Brick* b){
  if(b != NULL)
  {
    int xMid = b->xSize/2;
    int yMid = b->ySize/2;

    glColor3ub(b->red, b->green, b->blue);

    glBegin(GL_POLYGON);
      glVertex2f(b->xPos - xMid, b->yPos - yMid);
      glVertex2f(b->xPos + xMid, b->yPos - yMid);
      glVertex2f(b->xPos + xMid, b->yPos + yMid);
      glVertex2f(b->xPos - xMid, b->yPos + yMid);
    glEnd();
  }
}

void destroyBrick(Brick* brick){
    brick->xPos = 0;
    brick->yPos = 0;
    brick->xSize = 0;
    brick->ySize = 0;
    brick->visible = 0;
}

Brick ** initBrickTab(int brickTabWidth, int brickTabHeight, int* brickTabTypes, int brickHeight, int WINDOW_WIDTH, int WINDOW_HEIGHT){
    int yOrig = (int) WINDOW_HEIGHT/2.0 - (brickTabHeight*brickHeight)/2.0 + brickHeight/2.0;
    int brickWidth = (int) WINDOW_WIDTH/brickTabWidth;
    Brick ** brickTab;
    brickTab = malloc( brickTabHeight * sizeof(*brickTab));

    if (brickTab == NULL)
    {
      exit(EXIT_FAILURE);
    }
    int i;
    for (i = 0; i < brickTabHeight; ++i)
    {
      brickTab[i] = malloc( brickTabWidth * sizeof( *(brickTab[i]) ) );
        if (brickTab[i] == NULL)
        {
          exit(EXIT_FAILURE);
        }
    }
    int j;
    for (i = 0; i < brickTabHeight; ++i)
    {
      for (j = 0; j < brickTabWidth; ++j)
      {
        brickTab[i][j] = initBrick( (int) brickWidth/2 + j*brickWidth, yOrig + i*brickHeight, brickWidth, brickHeight, brickTabTypes[i*brickTabWidth+j], 1);
      }
    }
    return brickTab;
}

void printBrickTab(Brick ** brickTab, int brickTabHeight, int brickTabWidth){
    int i, j;
    for (i = 0; i < brickTabHeight; ++i)
    {
      for (j = 0; j < brickTabWidth; ++j)
      {
        printBrick(&brickTab[i][j]);

      }
    }
}

void freeBrickTab(Brick ** brickTab, int brickTabWidth) {
    int i;
    for (i = 0; i < brickTabWidth; ++i)
    {
        free(brickTab[i]);
    }
    free(brickTab);
}

void readBrickFile(int *brickTabHeight, int *brickTabWidth, int brickTabTypes[1024]) {
    FILE* levelFile = NULL;
    levelFile = fopen("level_10_10.txt", "r");
    if (levelFile == NULL) {
        printf("Fichier de niveau non trouvé.\n");
        exit(EXIT_FAILURE);
    }
    /**
    * Récupère les WIDTH et HEIGHT du tableau de Brick
    */
	fscanf(levelFile,"%d %d", brickTabWidth, brickTabHeight);

    /**
    * Stocke le contenu du fichier dans une grande chaine de caractères
    */
    char str[1024];
    fseek(levelFile, 1, SEEK_CUR);
    fgets (str, 1024, levelFile);

    /**
    * Divise la chaine de caracère en plusieurs chaines délimité par " "
    */
    char sep[] = " ";
    char *token;
    int var;

   token = strtok(str, sep);
   int i = 0;
   while( token != NULL )
   {
      /**
      * Formate la string en int
      */
      sscanf(token, "%d", &var);
      /**
      * Stocke le int dans le tableau de types
      */
      brickTabTypes[i++] = var;
      token = strtok(NULL, sep);
   }

   fclose(levelFile);
}
