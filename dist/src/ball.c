#include "game.h"

Ball initBall(float xPos, float yPos, float normeVitesse, float angleVitesse, float radius, int red, int green, int blue, Player * player){
  Ball ball;

  ball.xPos = xPos;
  ball.yPos = yPos;

  ball.normeVitesse = normeVitesse;
  ball.angleVitesse = angleVitesse;

  ball.radius = radius;

  ball.blue  = blue;
  ball.green = green;
  ball.red   = red;

  ball.texture = 0;
  ball.player  = player;

  return ball;
}

void printBall(Ball * ball){
  if(ball != NULL)
  {
    glPushMatrix();
      glTranslatef(ball->xPos, ball->yPos, 0);
      /*glScalef(ball->radius*2, ball->radius*2, 0);*/
      glColor3ub(ball->red, ball->green, ball->blue);
      drawCircle(1, ball->radius);
    glPopMatrix();

    float xTranslate = ball->normeVitesse * cos(ball->angleVitesse);
    float yTranslate = ball->normeVitesse * sin(ball->angleVitesse);
    setPositionBall(ball, ball->xPos + xTranslate, ball->yPos + yTranslate);
  }
}

void resetBall(Ball * ball){
  if(ball->player->id == 1){
    setPositionBall(ball, WINDOW_WIDTH/2, 3*WINDOW_HEIGHT/4);
    setAngleVitesseBall(ball, 2*M_PI - ball->angleVitesse);
  } else {
    setPositionBall(ball, WINDOW_WIDTH/2, 1*WINDOW_HEIGHT/4);
    setAngleVitesseBall(ball, - ball->angleVitesse - M_PI);
  }
}

void initDirectionBalls(Ball * ballPlayer1, Ball * ballPlayer2){
    if(ballPlayer1 != NULL){
      setNormeVitesseBall(ballPlayer1, 1);
      setAngleVitesseBall(ballPlayer1, - M_PI);
    }

    if(ballPlayer2 != NULL){
      setNormeVitesseBall(ballPlayer2, 1);
      setAngleVitesseBall(ballPlayer2, M_PI);
    }
}

void checkWallCollision(Ball * ball, Player * Player1, Player * Player2, int WINDOW_WIDTH, int WINDOW_HEIGHT){
  if (ball->xPos + ball->radius > WINDOW_WIDTH)
  {
    setAngleVitesseBall(ball, M_PI - ball->angleVitesse);
    setPositionBall(ball, WINDOW_WIDTH - ball->radius, ball->yPos);
  }
  if (ball->xPos - ball->radius < 0 )
  {
    setAngleVitesseBall(ball, M_PI - ball->angleVitesse);
    setPositionBall(ball, 0 + ball->radius, ball->yPos);
  }

  if (ball->yPos + ball->radius > WINDOW_HEIGHT)
  {
    Player1->life -= 1;
    resetBall(ball);
  }

  if (ball->yPos - ball->radius < 0)
  {
    Player2->life -= 1;
    resetBall(ball);
  }
}

void checkPaddlesCollision(Ball * ball, Paddle * paddle1, Paddle * paddle2){
  if(ball->yPos + ball->radius > paddle1->yPos - paddle1->ySize/2)
  {
    if(ball->xPos > paddle1->xPos - paddle1->xSize/2 && ball->xPos < paddle1->xPos + paddle1->xSize/2 )
    {
      setAngleVitesseBall(ball, 2*M_PI - ball->angleVitesse);
      setPositionBall(ball, ball->xPos, paddle1->yPos - paddle1->ySize);
    }
  }
  if(ball->yPos - ball->radius < paddle2->yPos + paddle2->ySize/2)
  {
    if (ball->xPos > paddle2->xPos - paddle2->xSize/2 && ball->xPos < paddle2->xPos + paddle2->xSize/2 )
    {
      setAngleVitesseBall(ball, 2*M_PI - ball->angleVitesse);
      setPositionBall(ball, ball->xPos, paddle2->yPos + paddle2->ySize);
    }
  }
}

void checkBrickCollision(Ball * ball, Brick * brick){
    /**
    * Ball va vers le bas
    */
    if (ball->angleVitesse >= 0 && ball->angleVitesse < M_PI ) {
        if(ball->yPos + ball->radius >= brick->yPos - brick->ySize/2 && ball->yPos - ball->radius < brick->yPos + brick->ySize/2)
        {
          if(ball->xPos >= brick->xPos - brick->xSize/2 && ball->xPos <= brick->xPos + brick->xSize/2 )
          {
            setAngleVitesseBall(ball, 2*M_PI - ball->angleVitesse);
            setPositionBall(ball, ball->xPos, ((float) brick->yPos - brick->ySize));
            handleBrick(brick, ball);

          }
        }
    }
    /**
    * Ball va vers le haut
    */
    else{
        if(ball->yPos - ball->radius < brick->yPos + brick->ySize/2 && ball->yPos + ball->radius >= brick->yPos - brick->ySize/2)
        {
          if (ball->xPos >= brick->xPos - brick->xSize/2 && ball->xPos <= brick->xPos + brick->xSize/2 )
          {
            setAngleVitesseBall(ball, 2*M_PI - ball->angleVitesse);
            setPositionBall(ball, ball->xPos, (float) brick->yPos + brick->ySize);
            handleBrick(brick, ball);
            
          }
        }
    }
}

void checkBricksCollision(Ball * ball, Brick ** brickTab, int brickTabHeight, int brickTabWidth){
    int i,j;
    for (i = 0; i < (brickTabHeight); i++) {
        for (j = 0; j < brickTabWidth; j++) {
            checkBrickCollision(ball, &brickTab[i][j]);
        }
    }
}

void setPositionBall(Ball * ball, float xPos, float yPos){
    ball->xPos = xPos;
    ball->yPos = yPos;
}

void setNormeVitesseBall(Ball * ball, float normeVitesse){
    ball->normeVitesse = normeVitesse;
}

void setAngleVitesseBall(Ball * ball, float angleVitesse){
    ball->angleVitesse = fmod(angleVitesse,2.0*M_PI);
}

void stopBall(Ball * ball){
    if(ball != NULL)
    {
        ball->normeVitesse = 0;
    }
}

void checkAllCollision(Player * player1, Player * player2, Ball * ballPlayer1, Ball * ballPlayer2, int WINDOW_WIDTH, int WINDOW_HEIGHT)
{
  checkWallCollision(ballPlayer1, player1, player2, WINDOW_WIDTH, WINDOW_HEIGHT);
  checkWallCollision(ballPlayer2, player1, player2, WINDOW_WIDTH, WINDOW_HEIGHT);

  checkPaddlesCollision(ballPlayer1, player1->paddle, player2->paddle);
  checkPaddlesCollision(ballPlayer2, player1->paddle, player2->paddle);
}
